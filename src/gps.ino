#include <SoftwareSerial.h>
const byte rxPin = 4;
const byte txPin = 2;

SoftwareSerial Serial1(rxPin, txPin); // RX, TX

unsigned char buffer[64];   // buffer array 
int count = 0;              // contador de caracteres del buffer

void setup()
{
    Serial.begin(9600);             // Serial de Arduino
    delay(1000); 
    Serial1.begin(9600);            // Software Serial GPS SIM39EA
                           
}

void loop()
{
    if (Serial1.available())              
    {
        while (Serial1.available())               // reading data into char array
        {
            buffer[count++] = Serial1.read();     // writing data into array
            if (count == 64)
                break;
        }

       
        for ( int j=0;j<count;j++)
        {
            Serial.write(buffer[j]);
        }
        clearBufferArray();              // borrar buffer array
        count = 0;    
        
    }

    //Se utiliza solo para enviarle comandos AT por el monitor Serial de Arduino
    if (Serial.available()){
        Serial1.write(Serial.read());
    }            
        
}

/**
    Limpiar Buffer

*/
void clearBufferArray()              // function to clear buffer array
{
    for (int i=0; i < count; i++)
    {
        buffer[i] = NULL;
    }                               // clear all index of array with command NULL
}